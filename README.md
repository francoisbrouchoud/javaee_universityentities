# Java EE Project - University manager

This application is a university management platform with various entities: professors, research projects, and courses. Five use cases have been developed to facilitate university administration. It was developed by Luca Del Buono and François Brouchoud, as part of the 635-1 Java EE course at HES-SO Valais-Wallis.

The project architecture is divided into three distinct layers, each utilizing specific Java EE technologies :

* Presentation: Layer: JSF, Facelets
* Service Layer: Stateless and Stateful EJB
* Persistence Layer: JPA, JPQL

## Main functionalities
|**Menu**|**Description**|
| :- | :- |
|**Manage professors**|<p>view and edit professor profile</p><p></p>|
|**Manage Research Projects**|assign a professor to an existing project|
|**Show project by date**|<p>list of all ongoing projects as of the entered date</p><p></p>|
|**Show professors total responsible budget**|<p>summary of the number of projects and the total budget a professor is responsible for</p><p></p>|
|**Manage course**|view and delete courses|
## Additional functionalities
- **Embedding** with *AcademicElement* which includes common fields between *Course* and *ResearchProject*
- **Stateful EJB**: history of projects assignment	
- **Security**: Staff can only view content, while admins can edit and delete content
## Configuration
### User configuration for security 

|Username|Password|Role|
| :- | :- | :- |
|StaffUser|StaffUser|staff|
|AdminUser|AdminUser|admin|

First, we need to add users to our application. To do this, use the script located at: rootJakartaEE\tools\wildfly-23.0.2.Final\bin\add-user.bat


### Seeding DB
The first time you launch the app as an AdminUser, you can seed the database by clicking on the "Seed DB" button.
Class diagram
![Une image contenant capture d’écran, texte, conception


## Class diagram
![ClassDiagram](universityClassDiagram.png)
