package ch.hevs.managedbeans;

import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ch.hevs.universityservice.University;
import ch.hevs.universityservice.UniversitySF;
import ch.hevs.businessobject.Course;
import ch.hevs.businessobject.Professor;
import ch.hevs.businessobject.ProfessorProjectsInfo;
import ch.hevs.businessobject.ResearchProject;
import ch.hevs.businessobject.enums.*;

@ManagedBean
public class AcademicBean {
	
	private University university;
	private UniversitySF universitySF;
	
	private List<Professor> professors;
	private List<ResearchProject> researchProjects;
	private Professor professor;
	
	private Long researchProjectId;
	private Long professorId;
	private String assignmentResult;
	
	private Set<Professor> researchProjectProfessors;
	private List<Course> courses;
	private Course course;
	private Set<String> courseRequirements;
	private Professor courseProfessorResponsible;
	
	private Set<Course> professorCourses;
	private Set<ResearchProject> professorResearchProjects;
	private Set<Course> professorCoursesResponsible;
	private Set<ResearchProject> professorResearchProjectsResponsible;
	private Set<Professor> courseProfessors;
	
	private String refDate;
	
	public AcademicBean() {
	 
	}
	public AcademicBean(University university, List<Professor> professors, Professor professor) {
		this.university = university;
		this.professors = professors;
		this.professor = professor;
	}
	
	/**
	 * Initialize lists and JNDI reference for EJB
	 * @throws NamingException
	 */
	@PostConstruct
	public void initialize() throws NamingException {
		InitialContext ctx = new InitialContext();
		university = (University) ctx.lookup("java:global/UNIVERSITY-WEB-EJB-PC-EPC-E-0.0.1-SNAPSHOT/UniversityBean!ch.hevs.universityservice.University");
		universitySF = (UniversitySF) ctx.lookup("java:global/UNIVERSITY-WEB-EJB-PC-EPC-E-0.0.1-SNAPSHOT/UniversityBeanSF!ch.hevs.universityservice.UniversitySF");
		
		this.professors = new ArrayList<Professor>();
		this.researchProjects = new ArrayList<ResearchProject>();
		this.courses = new ArrayList<Course>();
		this.professors = university.getProfessors();
		this.researchProjects = university.getResearchProjects();
		this.courses = university.getCourses();
	}
	
	/**
	 * Seed the database by calling method from EJB, check if already seeded.
	 * @return page depending on users role
	 */
	public String seed() {
		boolean isDBSeeded = universitySF.getIsDBSeeded();
		if(universitySF.isUserAdmin() && !isDBSeeded) {
			try {
				university.seed();
				this.initialize();
				universitySF.setIsDBSeeded();
			} catch (Exception e) {
				e.printStackTrace();	
			}
			return "welcomeUniverstiy";
		}else if (isDBSeeded) {
			return "dbAlreadySeeded";
		}
		else {
			return "notAllowed";
		}
	}
	
	/**
	 * Refresh the projects with the date
	 */
	public void refreshProjects() { 
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            java.util.Date utilDate = dateFormat.parse(refDate);
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            this.researchProjects = university.getActiveResearchProjects(sqlDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * Refresh the projects by querying the database
	 * @return ResearchProjectByDate page
	 */
	public String refreshResearchProjects() {
		this.researchProjects = university.getResearchProjects();
		return "researchProjectByDate";
	}
	
	/**
	 * Refresh professor's relation data
	 * @param p Professor to refresh
	 */
	public void refreshProfessor(Professor p) {
		this.professor = university.getProfessor(p.getId());
		this.professorCourses = this.professor.getCourses();
		this.professorResearchProjects = this.professor.getResearchProjects();
		this.professorCoursesResponsible = this.professor.getCoursesResponsability();
		this.professorResearchProjectsResponsible = this.professor.getResearchProjectsResponsability();
	}
    
	/**
	 * Open the edit professor form if the user is an admin
	 * @param p Professor to whom we want to open edit page
	 * @return EditProfessorForm page if success, notAllowed page otherwise
	 */
	public String openEditProfessor(Professor p) {
		if(universitySF.isUserAdmin()) {
			refreshProfessor(p);
			return "editProfessorForm";	
		}else {
			return "notAllowed";
		}
	}
	
	/**
	 * Perform the edition of the professor by calling the EJB method
	 * @return professorForm page
	 */
	public String performEdit() {
        university.editProfessor(this.professor);   
        this.professors = university.getProfessors();
		return "professorForm";
	}
	
	/**
	 * Open the professor detail page
	 * @param p Professor to whom we want to see details
	 * @return
	 */
	public String openDetailsProfessor(Professor p) {
		refreshProfessor(p);
		return "detailsProfessorForm";	
	}
	
	/**
	 * Build a map with the professor as a key and the research project informations as a value
	 * @return The map with the professor as a key and the research project informations as a value
	 */
	public Map<Professor, ProfessorProjectsInfo> getProfessorsWithBudgets() {
	    List<Professor> allProfessors = this.getProfessors();
	    int nbOfProjects; 
	    Map<Professor, ProfessorProjectsInfo> professorsWithBudgets = new HashMap<>();
	    for (Professor professor : allProfessors) {
	    	Double budget = university.getBudget(professor); 
	    	if (budget == null) {
	            budget = 0.0; 
	        }
	    	nbOfProjects = professor.getResearchProjectsResponsability().size();
	    	ProfessorProjectsInfo profProjectsInfo = new ProfessorProjectsInfo(budget, nbOfProjects);
	    	professorsWithBudgets.put(professor, profProjectsInfo);
	    }
	    return professorsWithBudgets;
	}
	
	/**
	 * Open the course details page
	 * @param c The course we want to see details
	 * @return detailsCourseForm page
	 */
	public String openDetailsCourse(Course c) {
		this.course = university.getCourse(c.getCourseId());
		this.courseRequirements = this.course.getRequirements();
		this.courseProfessorResponsible = this.course.getResponsibleProfessor();
		this.courseProfessors = this.course.getProfessors();
		return "detailsCourseForm";
	}
	
	/**
	 * Open the course deletion page
	 * @param c The course we want to delete
	 * @return deleteCourseForm page if the user is an admin, notAllowed page otherwise
	 */
	public String openDeleteCourse(Course c) {
		if(universitySF.isUserAdmin()) {
			this.course = c;
			return "deleteCourseForm";
		}else {
			return "notAllowed";
		}
	}
	
	/**
	 * Perform course deletion by calling the EJB method
	 * @return courseForm page
	 */
	public String performCourseDelete() {
		university.deleteCourse(course);
		this.courses = university.getCourses();
		return "courseForm";
	}
	
	/**
	 * Assign a professor to a research project by calling the stateful EJB method
	 * @return showAssignResult page if the user is an admin, notAllowed page otherwise
	 */
	public String assignProjectToProfessor() {
		Professor professor = university.getProfessor(professorId);
		ResearchProject researchProject = university.getResearchProject(researchProjectId);
		
		String assignResult = universitySF.performAssign(researchProject, professor);
		if(assignResult != null) {
			this.assignmentResult = assignResult;
			this.researchProjectProfessors = researchProject.getProfessors();
			return "showAssignResult";
		}
		else
			return "notAllowed";
	}
	
	/**
	 * Get history from stateful EJB
	 * @return
	 */
    public List<String> getHistory() {
    	List<String> history = this.universitySF.getHistory();
    	return history;
    }	
	
	public Long getResearchProjectId() {
		return researchProjectId;
	}

	public void setResearchProjectId(Long researchProjectId) {
		this.researchProjectId = researchProjectId;
	}

	public Long getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Long professorId) {
		this.professorId = professorId;
	}
	
    public String getRefDate() {
        return refDate;
    }

    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }
    
    public Set<Professor> getCourseProfessors() {
		return courseProfessors;
	}

	public void setCourseProfessors(Set<Professor> courseProfessors) {
		this.courseProfessors = courseProfessors;
	}
	
	public Set<Course> getProfessorCoursesResponsible() {
		return professorCoursesResponsible;
	}

	public void setProfessorCoursesResponsible(Set<Course> professorCoursesResponsible) {
		this.professorCoursesResponsible = professorCoursesResponsible;
	}

	public Set<ResearchProject> getProfessorResearchProjectsResponsible() {
		return professorResearchProjectsResponsible;
	}

	public void setProfessorResearchProjectsResponsible(Set<ResearchProject> professorResearchProjectsResponsible) {
		this.professorResearchProjectsResponsible = professorResearchProjectsResponsible;
	}

	public Set<ResearchProject> getProfessorResearchProjects() {
		return professorResearchProjects;
	}

	public void setProfessorResearchProjects(Set<ResearchProject> professorResearchProjects) {
		this.professorResearchProjects = professorResearchProjects;
	}

	public Set<Course> getProfessorCourses() {
		return professorCourses;
	}

	public void setProfessorCourses(Set<Course> professorCourses) {
		this.professorCourses = professorCourses;
	}

	public Professor getCourseProfessorResponsible() {
		return courseProfessorResponsible;
	}

	public void setCourseProfessorResponsible(Professor courseProfessorResponsible) {
		this.courseProfessorResponsible = courseProfessorResponsible;
	}

	public Set<String> getCourseRequirements() {
		return courseRequirements;
	}

	public void setCourseRequirements(Set<String> courseRequirements) {
		this.courseRequirements = courseRequirements;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public Set<Professor> getResearchProjectProfessors() {
		return researchProjectProfessors;
	}

	public void setResearchProjectProfessors(Set<Professor> researchProjectProfessors) {
		this.researchProjectProfessors = researchProjectProfessors;
	}

	public String getAssignmentResult() {
		return assignmentResult;
	}

	public void setAssignmentResult(String assignmentResult) {
		this.assignmentResult = assignmentResult;
	}
	
	public List<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(List<Professor> professors) {
		this.professors = professors;
	}
	
	public List<ResearchProject> getResearchProjects() {
		return researchProjects;
	}

	public void setResearchProjects(List<ResearchProject> researchProjects) {
		this.researchProjects = researchProjects;
	}
	
	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public InterestEnum[] getInterestEnumValues() {
	    return InterestEnum.values();
	}
	
	public TitleEnum[] getTitleEnumValues() {
	    return TitleEnum.values();
	}
	
	public FunctionEnum[] getFunctionEnumValues() {
	    return FunctionEnum.values();
	}
	
	public ResearchUnitEnum[] getResearchUnitEnumValues() {
	    return ResearchUnitEnum.values();
	}

}
