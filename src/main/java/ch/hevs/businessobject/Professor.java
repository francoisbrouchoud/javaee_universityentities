package ch.hevs.businessobject;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import ch.hevs.businessobject.enums.*;

import java.util.Date;
import java.util.Set;

@Entity
public class Professor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String firstname;
	private String lastname;
	@Enumerated(EnumType.STRING)
	private TitleEnum title;
	@Enumerated(EnumType.STRING)
	private FunctionEnum function;
	private String email;
	private String office;
	@Enumerated(EnumType.STRING)
	private InterestEnum interest;
	private String highestDiploma;
	private int activityRate;
	@Temporal(TemporalType.DATE)
	private Date birthdate;
	@Transient
	private int age;
	@Enumerated(EnumType.STRING)
	private ResearchUnitEnum researchUnit;
	private String imageUrl;
	private String linkedinUrl;
	private String publicationsUrl;
	
	// relations
	@ManyToMany(mappedBy = "professors", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private Set<Course> courses;
	@ManyToMany(mappedBy = "professors", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<ResearchProject> researchProjects;
	@OneToMany(mappedBy = "responsibleProfessor", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private Set<Course> coursesResponsability;
	@OneToMany(mappedBy = "responsibleProfessor", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<ResearchProject> researchProjectsResponsability;	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public TitleEnum getTitle() {
		return title;
	}
	public void setTitle(TitleEnum title) {
		this.title = title;
	}

	public FunctionEnum getFunction() {
		return function;
	}
	public void setFunction(FunctionEnum function) {
		this.function = function;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}

	public InterestEnum getInterest() {
		return interest;
	}
	public void setInterest(InterestEnum interest) {
		this.interest = interest;
	}

	public String getHighestDiploma() {
		return highestDiploma;
	}
	public void setHighestDiploma(String highestDiploma) {
		this.highestDiploma = highestDiploma;
	}

	public int getActivityRate() {
		return activityRate;
	}
	public void setActivityRate(int activityRate) {
		this.activityRate = activityRate;
	}

	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public ResearchUnitEnum getResearchUnit() {
		return researchUnit;
	}
	public void setResearchUnit(ResearchUnitEnum researchUnit) {
		this.researchUnit = researchUnit;
	}

	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}
	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getPublicationsUrl() {
		return publicationsUrl;
	}
	public void setPublicationsUrl(String publicationsUrl) {
		this.publicationsUrl = publicationsUrl;
	}
	
	public Set<ResearchProject> getResearchProjects() {
		return researchProjects;
	}
	public void setResearchProjects(Set<ResearchProject> researchProjects) {
		this.researchProjects = researchProjects;
	}
	
	public Set<Course> getCoursesResponsability() {
		return coursesResponsability;
	}
	public void setCoursesResponsability(Set<Course> coursesResponsability) {
		this.coursesResponsability = coursesResponsability;
	}
	
	public Set<ResearchProject> getResearchProjectsResponsability() {
		return researchProjectsResponsability;
	}
	public void setResearchProjectsResponsability(Set<ResearchProject> researchProjectsResponsability) {
		this.researchProjectsResponsability = researchProjectsResponsability;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
	
	// constructors
	public Professor() {
			
	}
	public Professor(String firstname, String lastname, TitleEnum title, FunctionEnum function, String email,
			String office, InterestEnum interest, String highestDiploma, int activityRate, Date birthdate,
			ResearchUnitEnum researchUnit, String imageUrl, String linkedinUrl, String publicationsUrl) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.title = title;
		this.function = function;
		this.email = email;
		this.office = office;
		this.interest = interest;
		this.highestDiploma = highestDiploma;
		this.activityRate = activityRate;
		this.birthdate = birthdate;
		this.researchUnit = researchUnit;
		this.imageUrl = imageUrl;
		this.linkedinUrl = linkedinUrl;
		this.publicationsUrl = publicationsUrl;
	}
	
	@Override
	public String toString() {
	    return "Professor{" +
	            "firstname='" + firstname + '\'' +
	            ", lastname='" + lastname + '\'' +
	            ", title=" + title +
	            ", function=" + function +
	            ", email='" + email + '\'' +
	            ", office='" + office + '\'' +
	            ", interest=" + interest +
	            ", highestDiploma='" + highestDiploma + '\'' +
	            ", activityRate=" + activityRate +
	            ", birthdate=" + birthdate +
	            ", researchUnit=" + researchUnit +
	            ", imageUrl='" + imageUrl + '\'' +
	            ", linkedinUrl='" + linkedinUrl + '\'' +
	            ", publicationsUrl='" + publicationsUrl + '\'' +
	            '}';
	}
}

