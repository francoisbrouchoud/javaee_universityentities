package ch.hevs.businessobject;

import java.util.Date;
import java.util.Set;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ResearchProject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long researchProjectId;
	private String description; 
	private String domain; 
	private String representative;
	private double budget; 
	@Temporal(TemporalType.DATE)
	private Date startDate;
	@Temporal(TemporalType.DATE)
	private Date endDate;
	@Embedded
	private AcademicElement academicElement;
	
	// relations
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Professor> professors;
	@ManyToOne
	private Professor responsibleProfessor;
	
	public Long getResearchProjectId() {
		return researchProjectId;
	}
	public void setResearchProjectId(Long researchProjectId) {
		this.researchProjectId = researchProjectId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getRepresentative() {
		return representative;
	}
	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	public double getBudget() {
		return budget;
	}
	public void setBudget(double budget) {
		this.budget = budget;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public AcademicElement getAcademicElement() {
		return academicElement;
	}
	public void setAcademicElement(AcademicElement academicElement) {
		this.academicElement = academicElement;
	}
	
	public Set<Professor> getProfessors() {
		return professors;
	}
	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}
	public Professor getResponsibleProfessor() {
		return responsibleProfessor;
	}
	public void setResponsibleProfessor(Professor responsibleProfessor) {
		this.responsibleProfessor = responsibleProfessor;
	}
	
	// constructors
	public ResearchProject() {
		
	}
	public ResearchProject(String description, String domain, String representative, double budget, Date startDate,
			Date endDate, AcademicElement academicElement) {
		super();
		this.description = description;
		this.domain = domain;
		this.representative = representative;
		this.budget = budget;
		this.startDate = startDate;
		this.endDate = endDate;
		this.academicElement = academicElement;
	}
	
	@Override
	public String toString() {
	    return "ResearchProject{" +
	            "researchProjectId=" + researchProjectId +
	            ", description='" + description + '\'' +
	            ", domain='" + domain + '\'' +
	            ", representative='" + representative + '\'' +
	            ", budget=" + budget +
	            ", startDate=" + startDate +
	            ", endDate=" + endDate +
	            ", academicElement=" + academicElement +
	            ", professors=" + professors +
	            '}';
	}

}
