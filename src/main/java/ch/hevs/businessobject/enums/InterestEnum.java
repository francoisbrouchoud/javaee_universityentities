package ch.hevs.businessobject.enums;

public enum InterestEnum {
	AI("Artificial Intelligence"),
	CLOUD("Could Computing"),
	NETWORK("Networks"),
	SECURTIY("Cybersecurity"),
	BI("Business Intelligence"),
	MIA("Medical Imaging Analysis"),
	INFRA("Infrastructure");
	
	public final String label;
	
	private InterestEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
	    return this.label;
	}
	
	@Override
    public String toString() {
        return label;
    }
	
	public static InterestEnum[] valuesAsString() {
		return values();
	}
}
