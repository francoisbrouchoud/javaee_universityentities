package ch.hevs.businessobject.enums;

public enum FunctionEnum{
	ASSISTANT("Assistant Professor"),
	ASSOCIATE("Associate Professor"),
	FULL("Full Professor");
	
	public final String label;
	
	private FunctionEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
	@Override
    public String toString() {
        return label;
    }
	
	public static FunctionEnum[] valuesAsString() {
		return values();
	}
}
