package ch.hevs.businessobject.enums;

public enum FormationTypeEnum {
	BSC("Bachelor"),
	MSC("Master"),
	FC("Continuous Formation");
	
	public final String label;
	
	private FormationTypeEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	@Override
    public String toString() {
        return label;
    }
	
	public static FormationTypeEnum[] valuesAsString() {
		return values();
	}
}
