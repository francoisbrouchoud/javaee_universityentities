package ch.hevs.businessobject.enums;

public enum ExamTypeEnum {
	ORAL("Oral Examination"),
	WRITTEN("Written Examination"),
	PROJECT("Project Examination");
	
	public final String label;
	
	private ExamTypeEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	@Override
    public String toString() {
        return label;
    }
	
	public static ExamTypeEnum[] valuesAsString() {
		return values();
	}
}
