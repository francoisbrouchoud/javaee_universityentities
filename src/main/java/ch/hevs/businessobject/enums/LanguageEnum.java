package ch.hevs.businessobject.enums;

public enum LanguageEnum {
	FR("Fran�ais"),
	DE("Deutsch"),
	EN("English"),
	IT("Italiano");
	
	public final String label;
	
	private LanguageEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	@Override
    public String toString() {
        return label;
    }
	
	public static LanguageEnum[] valuesAsString() {
		return values();
	}
	
}
