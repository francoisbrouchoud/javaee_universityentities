package ch.hevs.businessobject.enums;

public enum ResearchUnitEnum {
	AISLAB("Applied Intelligent Systems Lab"),
	CONEX("Connected Experience Lab"),
	DUDE("Data Understanding Data Explained"),
	EASI("Energy Application and Systems Integration"),
	MEDGIFT("MedGIFT");
	
	public final String label;
	
	private ResearchUnitEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	@Override
    public String toString() {
        return label;
    }
	
	public static ResearchUnitEnum[] valuesAsString() {
		return values();
	}
}
