package ch.hevs.businessobject.enums;

public enum TitleEnum{
	PHD("PhD"),
	MSC("MSc"),
	BSC("BSc");
	
	public final String label;
	
	private TitleEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	@Override
    public String toString() {
        return label;
    }
	
	public static TitleEnum[] valuesAsString() {
		return values();
	}
}