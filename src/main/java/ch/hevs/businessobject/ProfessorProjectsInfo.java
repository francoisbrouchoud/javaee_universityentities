package ch.hevs.businessobject;

public class ProfessorProjectsInfo {
	
	private Double budget;
	private Integer nbOfProjects;
	 
	public ProfessorProjectsInfo(Double budget, Integer nbOfProjects) {
		this.budget = budget;
		this.nbOfProjects = nbOfProjects;
	}
	
	public Double getBudget() {
		return budget;
	}
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	public Integer getNbOfProjects() {
		return nbOfProjects;
	}
	public void setNbOfProjects(Integer nbOfProjects) {
		this.nbOfProjects = nbOfProjects;
	}
}
