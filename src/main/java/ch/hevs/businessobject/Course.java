package ch.hevs.businessobject;

import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import ch.hevs.businessobject.enums.*;

@Entity
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long courseId;
	@Enumerated(EnumType.STRING)
	private FormationTypeEnum formationType;
	private int credits;
	@Enumerated(EnumType.STRING)
	private LanguageEnum language; 
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> requirements;
	@Enumerated(EnumType.STRING)
	private ExamTypeEnum examType;
	private String passCriteria;
	@Embedded
	private AcademicElement academicElement;
	
	// relations
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Professor> professors;
	@ManyToOne
	private Professor responsibleProfessor;
	
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public FormationTypeEnum getFormationType() {
		return formationType;
	}
	public void setFormationType(FormationTypeEnum formationType) {
		this.formationType = formationType;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	public LanguageEnum getLanguage() {
		return language;
	}
	public void setLanguage(LanguageEnum language) {
		this.language = language;
	}
	public Set<String> getRequirements() {
		return requirements;
	}
	public void setRequirements(Set<String> requirements) {
		this.requirements = requirements;
	}
	public ExamTypeEnum getExamType() {
		return examType;
	}
	public void setExamType(ExamTypeEnum examType) {
		this.examType = examType;
	}
	public String getPassCriteria() {
		return passCriteria;
	}
	public void setPassCriteria(String passCriteria) {
		this.passCriteria = passCriteria;
	}
	public AcademicElement getAcademicElement() {
		return academicElement;
	}
	public void setAcademicElement(AcademicElement academicElement) {
		this.academicElement = academicElement;
	}
	public Set<Professor> getProfessors() {
		return professors;
	}
	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}
	public Professor getResponsibleProfessor() {
		return responsibleProfessor;
	}
	public void setResponsibleProfessor(Professor responsibleProfessor) {
		this.responsibleProfessor = responsibleProfessor;
	}
	
	// constructors
	public Course() {
		
	}
	public Course(FormationTypeEnum formationType, int credits, LanguageEnum language,
			Set<String> requirements, ExamTypeEnum examType, String passCriteria, AcademicElement academicElement) {
		this.formationType = formationType;
		this.credits = credits;
		this.language = language;
		this.requirements = requirements;
		this.examType = examType;
		this.passCriteria = passCriteria;
		this.academicElement = academicElement;
	}
}
