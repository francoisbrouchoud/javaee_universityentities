package ch.hevs.businessobject;

import javax.persistence.Embeddable;

@Embeddable
public class AcademicElement {
	
	private String name; 
	private String mainCompetency;
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getMainCompetency() {
		return mainCompetency;
	}
	public void setMainCompetency(String mainCompetency) {
		this.mainCompetency = mainCompetency;
	}
	
	public AcademicElement() {
		
	}
	public AcademicElement(String name, String mainCompetency) {
		this.name = name;
		this.mainCompetency = mainCompetency;
	}
	
}
