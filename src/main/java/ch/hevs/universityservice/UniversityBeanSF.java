package ch.hevs.universityservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import ch.hevs.businessobject.Professor;
import ch.hevs.businessobject.ResearchProject;

@Stateful
@RolesAllowed(value = { "staff", "admin" })
public class UniversityBeanSF implements UniversitySF{
	
	@PersistenceContext(name = "UniversityPU", type=PersistenceContextType.TRANSACTION)
	private EntityManager em;
	
	@Resource 
	private SessionContext ctx;
	
	private List<String> history;
	private boolean isDBSeeded;
	
	/**
	 * Init attributes
	 */
	@PostConstruct
	private void init() {
		this.history = new ArrayList<String>();
		this.isDBSeeded = false;
	}
	
	public List<String> getHistory(){
		return this.history;
	}
	
	/**
	 * Perform the assignment of a professor to a research project
	 */
	public String performAssign(ResearchProject rp, Professor p) {
		String result;
		Set<Professor> professors = new HashSet<>();
		professors= rp.getProfessors();
		// Add the professor to the list in order to merge it
		professors.add(p);
		rp.setProfessors(professors);

		if(ctx.isCallerInRole("admin")) {
			ResearchProject mergedRp = em.merge(rp);
			if(rp.getProfessors().size() == mergedRp.getProfessors().size()) {
				result = p.getFirstname() + " " + p.getLastname() + " has been assigned to " + rp.getAcademicElement().getName();
				this.history.add(result);
			}else {
				result = p.getFirstname() + " " + p.getLastname() + " is already assigned to " + rp.getAcademicElement().getName();
				// Remove the professor in case he is already assigned to the project
				professors.remove(p);
			}
			return result;
		}
		return null;
	}

	/**
	 * Returns whether a user is an admin or not
	 */
	@Override
	public boolean isUserAdmin() {
		if(ctx.isCallerInRole("admin")) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public boolean getIsDBSeeded() {
		return isDBSeeded;
	}

	@Override
	public void setIsDBSeeded() {
		isDBSeeded = true;
	}

}
