package ch.hevs.universityservice;

import java.util.List;
import javax.ejb.Local;
import ch.hevs.businessobject.Professor;
import ch.hevs.businessobject.ResearchProject;

@Local
public interface UniversitySF {
	String performAssign(ResearchProject rp, Professor p);
	List<String> getHistory();
	boolean isUserAdmin();
	boolean getIsDBSeeded();
	void setIsDBSeeded();
}
