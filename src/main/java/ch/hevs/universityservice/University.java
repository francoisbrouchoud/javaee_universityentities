package ch.hevs.universityservice;

import java.sql.Date;
import java.util.List;

import javax.ejb.Local;

import ch.hevs.businessobject.Course;
import ch.hevs.businessobject.Professor;
import ch.hevs.businessobject.ResearchProject;

@Local
public interface University {
	Professor getProfessor(Long id); 
	List<Professor> getProfessors();
	Double getBudget(Professor professor);
	void editProfessor(Professor professor);
	
	Course getCourse(Long id);
	List<Course> getCourses();
	void deleteCourse(Course c);
	
	ResearchProject getResearchProject(Long id);
	List<ResearchProject> getResearchProjects();
	List<ResearchProject> getActiveResearchProjects(Date refDate);
	
	void seed();
}
