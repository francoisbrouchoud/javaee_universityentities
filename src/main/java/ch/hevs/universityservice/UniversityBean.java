package ch.hevs.universityservice;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import ch.hevs.businessobject.AcademicElement;
import ch.hevs.businessobject.Course;
import ch.hevs.businessobject.Professor;
import ch.hevs.businessobject.ResearchProject;
import ch.hevs.businessobject.enums.*;



// Stateless Session Bean with business method to manage professors, researchProjects and courses
@Stateless
@RolesAllowed(value = { "staff", "admin" })
public class UniversityBean implements University{
	
	@PersistenceContext(name = "UniversityPU", type=PersistenceContextType.TRANSACTION)
	private EntityManager em;
	
	@Resource 
	private SessionContext ctx;
	
	// To populate all the sample data
	public void seed() {	
		Calendar calendar = Calendar.getInstance();
		calendar.set(1970, Calendar.JANUARY, 1); 
		Date birthdate1 = calendar.getTime();
		calendar.set(1980, Calendar.JANUARY, 1); 
		Date birthdate2 = calendar.getTime();
		calendar.set(1975, Calendar.JANUARY, 1); 
		Date birthdate3 = calendar.getTime();
		
		Professor prof1 = new Professor(
			    "Michael", 
			    "Schumacher", 
			    TitleEnum.PHD, 
			    FunctionEnum.FULL, 
			    "michael.schumacher@hevs.ch", 
			    "TP3.N137", 
			    InterestEnum.CLOUD, 
			    "PhD in Computer Science", 
			    100, 
			    birthdate1, 
			    ResearchUnitEnum.AISLAB, 
			    "http://test.com/img.jpeg", 
			    "http://linkedin.com/in/test", 
			    "http://hevs.ch" 
		);
		em.persist(prof1);
		
		Professor prof2 = new Professor(
			    "Jean-Luc", 
			    "Beuchat", 
			    TitleEnum.PHD, 
			    FunctionEnum.ASSOCIATE, 
			    "jean.luc.beuchat@hevs.ch", 
			    "TP3.N157", 
			    InterestEnum.SECURTIY, 
			    "PhD in Computer Science", 
			    100, 
			    birthdate2, 
			    ResearchUnitEnum.CONEX, 
			    "http://test.com/img.jpeg", 
			    "http://linkedin.com/in/test", 
			    "http://hevs.ch" 
		);
		em.persist(prof2);
		
		Professor prof3 = new Professor(
			    "Henning", 
			    "Muller", 
			    TitleEnum.PHD, 
			    FunctionEnum.FULL, 
			    "henning.muller@hevs.ch", 
			    "TP3.N147", 
			    InterestEnum.AI, 
			    "PhD in Computer Science", 
			    100, 
			    birthdate3, 
			    ResearchUnitEnum.MEDGIFT, 
			    "http://test.com/img.jpeg", 
			    "http://linkedin.com/in/test", 
			    "http://hevs.ch" 
		);
		em.persist(prof3);
		
		Professor prof4 = new Professor(
			    "David", 
			    "Russo", 
			    TitleEnum.MSC, 
			    FunctionEnum.ASSOCIATE, 
			    "david.russo@hevs.ch", 
			    "TP3.N147", 
			    InterestEnum.NETWORK, 
			    "M.Sc. in Information and Computer Sciences", 
			    80, 
			    birthdate3, 
			    ResearchUnitEnum.CONEX, 
			    "http://test.com/img.jpeg", 
			    "http://linkedin.com/in/test", 
			    "http://hevs.ch" 
		);
		em.persist(prof4);

		AcademicElement academicElement1 = new AcademicElement("Software Design Pattern", "Java");
		AcademicElement academicElement2 = new AcademicElement("Mobile development", "Cloud");
		AcademicElement academicElement3 = new AcademicElement("POO", "Java");
		AcademicElement academicElement4 = new AcademicElement("OpenEHealth", "Cloud");
		AcademicElement academicElement5 = new AcademicElement("Data secure access", "Network");
		AcademicElement academicElement6 = new AcademicElement("AI Cancer recognition", "AI");
		AcademicElement academicElement7 = new AcademicElement("Energy livedata Analysis", "Big Data");
		
		
		Set<String> requirementsCourse1 = new HashSet<>();
		requirementsCourse1.add("POO");
		requirementsCourse1.add("Good in Java prog");
	    Course course1 = new Course(
	    		FormationTypeEnum.BSC,
	    		5,
	    		LanguageEnum.FR,
	    		requirementsCourse1,
	    		ExamTypeEnum.WRITTEN,
	    		">=4",
	    		academicElement1		
	    );
	    
		Set<String> requirementsCourse2 = new HashSet<>();
		requirementsCourse2.add("SQL");
		requirementsCourse2.add("Good in Java prog");
	    Course course2 = new Course(
	    		FormationTypeEnum.BSC,
	    		3,
	    		LanguageEnum.EN,
	    		requirementsCourse2,
	    		ExamTypeEnum.PROJECT,
	    		">=4",
	    		academicElement2		
	    );
	    
		Set<String> requirementsCourse3 = new HashSet<>();
		requirementsCourse3.add("Fond prog Java required");
		requirementsCourse3.add("Modélisation des SI");	    
	    Course course3 = new Course(
	    		FormationTypeEnum.BSC,
	    		3,
	    		LanguageEnum.FR,
	    		requirementsCourse3,
	    		ExamTypeEnum.WRITTEN,
	    		">=4",
	    		academicElement3		
	    );
	        
	    course1.setResponsibleProfessor(prof1);
	    Set<Professor> professors = new HashSet<>();
	    professors.add(prof1);
	    course1.setProfessors(professors);
	    
	    course2.setResponsibleProfessor(prof1);
	    Set<Professor> professors2 = new HashSet<>();
	    professors2.add(prof1);
	    professors2.add(prof3);
	    course2.setProfessors(professors2);
	    
	    course3.setResponsibleProfessor(prof2);
	    Set<Professor> professors3 = new HashSet<>();
	    professors3.add(prof2);
	    professors3.add(prof4);
	    course3.setProfessors(professors3);

	    em.persist(course1);
	    em.persist(course2);
	    em.persist(course3);
	    
	    calendar.set(2022, Calendar.JANUARY, 1); 
		Date start1 = calendar.getTime();
		calendar.set(2023, Calendar.JANUARY, 1); 
		Date start2 = calendar.getTime();
		calendar.set(2024, Calendar.JANUARY, 1); 
		Date end1 = calendar.getTime();
		calendar.set(2023, Calendar.MAY, 1); 
		Date end2 = calendar.getTime();
		calendar.set(2023, Calendar.MARCH, 1); 
		Date start3 = calendar.getTime();
		calendar.set(2024, Calendar.JULY, 1); 
		Date end3 = calendar.getTime();
		calendar.set(2022, Calendar.DECEMBER, 1); 
		Date start4 = calendar.getTime();
		calendar.set(2023, Calendar.NOVEMBER, 1); 
		Date end4 = calendar.getTime();
	    
	    ResearchProject researchProject1 = new ResearchProject(
	    		"This research project is focused on medical data.",
	    		"Open Data",
	    		"Hopital VS",
	    		35000,
	    		start1,
	    		end1,
	    		academicElement4
	    );
	    
	    ResearchProject researchProject2 = new ResearchProject(
	    		"\r\n"
	    		+ "This research project aims to develop a secure tunneling mechanism for accessing sensitive data.",
	    		"IT Security",
	    		"Netplus",
	    		15000,
	    		start2,
	    		end2,
	    		academicElement5
	    );
	    
	    ResearchProject researchProject3 = new ResearchProject(
	    	    "This research aims to decect hearth cancer with AI applied on images.",
	    	    "Artificial Intelligence",
	    	    "HUG",
	    	    50000,
	    	    start3,
	    	    end3,
	    	    academicElement6
	    	);

    	ResearchProject researchProject4 = new ResearchProject(
	    	    "This research project aims to predict energy shortage with live data from GRD.",
	    	    "Data Analysis",
	    	    "Oiken",
	    	    40000,
	    	    start4,
	    	    end4,
	    	    academicElement7
    		);
	        
	    researchProject1.setResponsibleProfessor(prof3);
	    Set<Professor> professors4 = new HashSet<>();
	    professors4.add(prof3);
	    professors4.add(prof1);
	    researchProject1.setProfessors(professors4);
	    
	    researchProject2.setResponsibleProfessor(prof2);
	    Set<Professor> professors5 = new HashSet<>();
	    professors5.add(prof2);
	    professors5.add(prof4);
	    researchProject2.setProfessors(professors5);
	    
	    researchProject3.setResponsibleProfessor(prof3);
	    Set<Professor> professors6 = new HashSet<>();
	    professors6.add(prof2);
	    professors6.add(prof3);
	    researchProject3.setProfessors(professors6);
	    
	    researchProject4.setResponsibleProfessor(prof1);
	    Set<Professor> professors7 = new HashSet<>();
	    professors7.add(prof1);
	    professors7.add(prof2);
	    professors7.add(prof4);
	    researchProject4.setProfessors(professors7);
	   
	    em.persist(researchProject1);
	    em.persist(researchProject2);
	    em.persist(researchProject3);
	    em.persist(researchProject4);
	}

	/**
     * To get a professor by id
     * @param id of professor to get
     * @return Professor 
     */
	@Override
	public Professor getProfessor(Long id) {
		Query query =  em.createQuery("FROM Professor p WHERE p.id=:id");
		query.setParameter("id", id);
		return (Professor) query.getSingleResult();
	}

	/**
     * To get the list of professors
     * @return List<Professor> the list of all professors
     */
	@Override
	public List<Professor> getProfessors() {
		return em.createQuery("FROM Professor").getResultList();
	}

	/**
     * To calculate the total budget a professor is responsible for its research projects.
     * @param professor : for who we calculate the budget
     * @return Double : total budget for its projects
     */
	@Override
	public Double getBudget(Professor professor) {
		Query query = em.createQuery("SELECT sum(rp.budget) FROM ResearchProject rp WHERE rp.responsibleProfessor=:professor");
		query.setParameter("professor", professor);
		return (Double) query.getSingleResult();
	}
	
	/**
	 * To update professor with the edited parameters
     * @param professor with updated details.
	 */
	@Override
	public void editProfessor(Professor professor) {
		em.merge(professor);
	}

	/**
	 * To get a course by id
	 * @param id of the course
     * @return Course 
	 */
	@Override
	public Course getCourse(Long id) {
		Query query = em.createQuery("FROM Course c WHERE c.courseId=:id");
		query.setParameter("id", id);
		return (Course) query.getSingleResult();
	}

	/**
     * To get all courses
     * @return List<Course> list of all courses
     */
	@Override
	public List<Course> getCourses() {
		return em.createQuery("FROM Course").getResultList();
	}

	/**
     * To get a research project by id.
     * @param id of the research project 
     * @return ResearchProject 
     */
	@Override
	public ResearchProject getResearchProject(Long id) {
		Query query =  em.createQuery("FROM ResearchProject rp WHERE rp.researchProjectId=:id");
		query.setParameter("id", id);
		return (ResearchProject) query.getSingleResult();
	}
	
	
	/**
	 * To get a list of projects
	 * @return list of all researchProjects
	 */
	public List<ResearchProject> getResearchProjects() {
		return em.createQuery("FROM ResearchProject").getResultList();
	}

	/**
	 * To get all projects that are currently active with a reference date given
	 * @param refDate represent a date to get the projects active at this date 
	 * @return list of all researchProjects active at this refdate
	 */
	@Override
	public List<ResearchProject> getActiveResearchProjects(java.sql.Date refDate) {
		 Query query = em.createQuery("FROM ResearchProject rp WHERE :refDate BETWEEN rp.startDate AND rp.endDate");
		 query.setParameter("refDate", refDate);
		 return (List<ResearchProject>) query.getResultList();
	}

	
	/**
	 * To delete a course from the database. 
	 * @param c : the Course object to be deleted
	 */
	@Override
	public void deleteCourse(Course c) {
		em.remove(em.contains(c) ? c : em.merge(c));
	}

}
